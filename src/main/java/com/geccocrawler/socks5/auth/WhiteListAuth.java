package com.geccocrawler.socks5.auth;

public interface WhiteListAuth {
	boolean auth(String ip);
}

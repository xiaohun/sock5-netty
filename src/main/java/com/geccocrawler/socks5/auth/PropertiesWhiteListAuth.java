package com.geccocrawler.socks5.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesWhiteListAuth implements WhiteListAuth {
    private static final Logger logger = LoggerFactory.getLogger(PropertiesWhiteListAuth.class);

    private static Properties properties;

    static {
        properties = new Properties();
        try {
            File file = new File(System.getProperty("user.dir") + File.separatorChar + "whitelist.properties");
            if (file.exists()) {
                properties.load(new FileInputStream(file));
            } else {
                properties.load(PropertiesWhiteListAuth.class.getResourceAsStream("/whitelist.properties"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean auth(String ip) {
        logger.debug("当前IP: {}", ip);
        String whiteListIp = properties.getProperty(ip);
        return whiteListIp != null;
    }

}

package com.geccocrawler.socks5.handler.ss5;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;

public class UdpProxyRequestHandler extends SimpleChannelInboundHandler<DatagramPacket> {
    private static final Logger logger = LoggerFactory.getLogger(UdpProxyRequestHandler.class);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DatagramPacket msg) throws Exception {
        byte[] byteBuf = new byte[msg.content().writerIndex()];
        msg.content().readBytes(byteBuf);
        System.out.println( new String(byteBuf, StandardCharsets.UTF_8));
    }
}

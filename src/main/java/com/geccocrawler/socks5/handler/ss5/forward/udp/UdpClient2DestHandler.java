package com.geccocrawler.socks5.handler.ss5.forward.udp;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.DatagramPacket;

/**
 * Created with IntelliJ IDEA
 * User: wangyong
 * Date: 2019-04-03
 * Time: 11:54
 * Description: 将客户端的消息转发给目标服务器端
 */
public class UdpClient2DestHandler extends SimpleChannelInboundHandler<ByteBuf> {
    private static final Logger logger = LoggerFactory.getLogger(UdpClient2DestHandler.class);

    private ChannelFuture destChannelFuture;

    public UdpClient2DestHandler(ChannelFuture destChannelFuture) {
        this.destChannelFuture = destChannelFuture;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf chmsg) throws Exception {
        logger.trace("将客户端的消息转发给目标服务器端");
        destChannelFuture.channel().writeAndFlush(chmsg);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        logger.trace("客户端断开连接");
        destChannelFuture.channel().close();
    }
}

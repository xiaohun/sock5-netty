package com.geccocrawler.socks5.handler.ss5.forward;

import io.netty.util.Attribute;
import io.netty.util.AttributeKey;

import java.net.InetSocketAddress;

/**
 * Created with IntelliJ IDEA
 * User: wangyong
 * Date: 2019-04-03
 * Time: 13:45
 * Description:
 */
public class Common {
    public static final AttributeKey<InetSocketAddress> REMOTE_DES = AttributeKey.valueOf("REMOTE_DES");
}

package com.geccocrawler.socks5.handler.ss5.forward.tcp;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with IntelliJ IDEA
 * User: wangyong
 * Date: 2019-04-03
 * Time: 11:54
 * Description: 将目标服务器信息转发给客户端
 */
public class TcpDest2ClientHandler extends ChannelInboundHandlerAdapter {
    private static final Logger logger = LoggerFactory.getLogger(TcpDest2ClientHandler.class);

    private ChannelHandlerContext clientChannelContext;

    public TcpDest2ClientHandler(ChannelHandlerContext clientChannelContext) {
        this.clientChannelContext = clientChannelContext;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx2, Object destMsg) throws Exception {
        logger.trace("将目标服务器信息转发给客户端");
        clientChannelContext.writeAndFlush(destMsg);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx2) throws Exception {
        logger.trace("目标服务器断开连接");
        clientChannelContext.channel().close();
    }
}

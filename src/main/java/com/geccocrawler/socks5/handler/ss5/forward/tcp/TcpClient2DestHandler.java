package com.geccocrawler.socks5.handler.ss5.forward.tcp;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with IntelliJ IDEA
 * User: wangyong
 * Date: 2019-04-03
 * Time: 11:54
 * Description: 将客户端的消息转发给目标服务器端
 */
public class TcpClient2DestHandler extends ChannelInboundHandlerAdapter {
    private static final Logger logger = LoggerFactory.getLogger(TcpClient2DestHandler.class);

    private ChannelFuture destChannelFuture;

    public TcpClient2DestHandler(ChannelFuture destChannelFuture) {
        this.destChannelFuture = destChannelFuture;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        logger.trace("将客户端的消息转发给目标服务器端");
        destChannelFuture.channel().writeAndFlush(msg);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        logger.info("客户端断开连接");
        destChannelFuture.channel().close();
    }
}

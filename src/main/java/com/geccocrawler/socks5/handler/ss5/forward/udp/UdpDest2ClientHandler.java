package com.geccocrawler.socks5.handler.ss5.forward.udp;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.DatagramPacket;

/**
 * Created with IntelliJ IDEA
 * User: wangyong
 * Date: 2019-04-03
 * Time: 11:54
 * Description: 将目标服务器信息转发给客户端
 */
public class UdpDest2ClientHandler extends SimpleChannelInboundHandler<DatagramPacket> {
    private static final Logger logger = LoggerFactory.getLogger(UdpDest2ClientHandler.class);

    private ChannelHandlerContext clientChannelContext;

    public UdpDest2ClientHandler(ChannelHandlerContext clientChannelContext) {
        this.clientChannelContext = clientChannelContext;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DatagramPacket destMsg) throws Exception {
        logger.trace("将目标服务器信息转发给客户端");
        clientChannelContext.channel().writeAndFlush(destMsg.getData());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx2) throws Exception {
        logger.trace("目标服务器断开连接");
        clientChannelContext.channel().close();
    }
}
